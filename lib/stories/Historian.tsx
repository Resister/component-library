import React from "react";
import { Box, Button, TextField, Typography } from "@mui/material";

interface HistorianProps {
  title: string;
}

export function Historian({ title }: HistorianProps): JSX.Element {
  return (
    <Box
      style={{
        display: "flex",
        flexDirection: "column",
        height: 200,
        justifyContent: "space-between",
      }}
    >
      <Typography variant="h3">{title}</Typography>
      <TextField placeholder="Query" />
      <Button variant="contained">Send</Button>
    </Box>
  );
}
