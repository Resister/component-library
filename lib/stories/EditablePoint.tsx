import React from "react";
import {
  Box,
  Button,
  Card,
  FormControl,
  InputLabel,
  MenuItem,
  Select,
  SelectChangeEvent,
  Typography,
} from "@mui/material";

const deviceTypes = ["MET", "NCU"];
const deviceIds = ["MET1", "MET2", "MET3", "MET4", "MET5", "MET6", "MET7"];
const points = [
  "Pressure",
  "Wind Speed",
  "Temperature",
  "Humidity",
  "Rainfall",
];

const style = {
  width: "100%",
  display: "flex",
  padding: 15,
  alignItems: "center",
  justifyContent: "space-between",
};

type BasicSelectProps = {
  options: string[];
  label: string;
  setPoint?: React.Dispatch<React.SetStateAction<string>>;
  point?: string;
};

function BasicSelect({
  options,
  label,
  setPoint,
  point,
}: BasicSelectProps): JSX.Element {
  const handleChange = (event: SelectChangeEvent): void => {
    if (setPoint) {
      setPoint(event.target.value as string);
    }
  };

  return (
    <Box sx={{ minWidth: 120 }}>
      <FormControl fullWidth>
        <InputLabel id="demo-simple-select-label">{label}</InputLabel>
        <Select
          labelId="demo-simple-select-label"
          id="demo-simple-select"
          value={point}
          label={label}
          onChange={handleChange}
        >
          {options.map((option) => (
            <MenuItem key={option} value={option}>
              {option}
            </MenuItem>
          ))}
        </Select>
      </FormControl>
    </Box>
  );
}
export function EditablePoint(): JSX.Element {
  const [mode, setMode] = React.useState<"view" | "edit">("view");
  const [point, setPoint] = React.useState<string>("Irradiance");

  return (
    <Card
      style={{
        width: 500,
        height: 86,
        display: "flex",
      }}
    >
      {mode === "view" ? (
        <Box style={style}>
          <Box
            style={{
              display: "flex",
              width: "75%",
              justifyContent: "space-between",
            }}
          >
            <Typography variant="body1">{point}</Typography>
            <Typography variant="body1">{(Math.random() * 101) | 0}</Typography>
          </Box>
          <Box mt={-1}>
            <Button onClick={(): void => setMode("edit")}>Edit</Button>
          </Box>
        </Box>
      ) : (
        <Box style={style}>
          <BasicSelect label="DeviceType" options={deviceTypes} />
          <BasicSelect label="DeviceIds" options={deviceIds} />
          <BasicSelect
            point={point}
            setPoint={setPoint}
            label="Point"
            options={points}
          />
          <Box mt={-1}>
            <Button onClick={(): void => setMode("view")}>View</Button>
          </Box>
        </Box>
      )}
    </Card>
  );
}
