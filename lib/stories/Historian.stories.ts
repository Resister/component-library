import type { Meta, StoryObj } from "@storybook/react";

import { Historian } from "./Historian";

// More on how to set up stories at: https://storybook.js.org/docs/7.0/react/writing-stories/introduction
const meta: Meta<typeof Historian> = {
  title: "Example/Historian",
  component: Historian,
  tags: ["docsPage"],
  argTypes: {},
};

export default meta;
type Story = StoryObj<typeof Historian>;

// More on writing stories with args: https://storybook.js.org/docs/7.0/react/writing-stories/args
export const Main: Story = {
  args: {
    title: "Historian",
  },
};
