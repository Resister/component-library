import type { Meta, StoryObj } from "@storybook/react";

import { EditablePoint } from "./EditablePoint";

const meta: Meta<typeof EditablePoint> = {
  title: "Example/EditablePoint",
  component: EditablePoint,
  tags: ["docsPage"],
  argTypes: {},
};

export default meta;
type Story = StoryObj<typeof EditablePoint>;
// More on writing stories with args: https://storybook.js.org/docs/7.0/react/writing-stories/args
export const Main: Story = {
  args: {},
};
