import { Theme } from "@mui/material/styles";

// ----------------------------------------------------------------------

// todo: add interface
export default function Breadcrumbs(theme: Theme): unknown {
  return {
    MuiBreadcrumbs: {
      styleOverrides: {
        separator: {
          marginLeft: theme.spacing(2),
          marginRight: theme.spacing(2),
        },
      },
    },
  };
}
