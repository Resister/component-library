import { Theme } from "@mui/material/styles";

// ----------------------------------------------------------------------
// todo: add interface
export default function Timeline(theme: Theme): unknown {
  return {
    MuiTimelineDot: {
      styleOverrides: {
        root: {
          boxShadow: "none",
        },
      },
    },

    MuiTimelineConnector: {
      styleOverrides: {
        root: {
          backgroundColor: theme.palette.divider,
        },
      },
    },
  };
}
