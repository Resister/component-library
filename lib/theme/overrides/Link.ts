// ----------------------------------------------------------------------

// todo: add interface
export default function Link(): unknown {
  return {
    MuiLink: {
      defaultProps: {
        underline: "hover",
      },
    },
  };
}
