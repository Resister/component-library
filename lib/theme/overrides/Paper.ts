import { Theme } from "@mui/material/styles";

// ----------------------------------------------------------------------

// todo: add interface
export default function Paper(theme: Theme): unknown {
  return {
    MuiPaper: {
      defaultProps: {
        elevation: 0,
      },

      variants: [
        {
          props: { variant: "outlined" },
          style: { borderColor: theme.palette.grey[500_12] },
        },
      ],

      styleOverrides: {
        root: {
          backgroundImage: "none",
        },
      },
    },
  };
}
