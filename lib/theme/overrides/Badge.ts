// ----------------------------------------------------------------------

// todo: add interface
export default function Badge(): unknown {
  return {
    MuiBadge: {
      styleOverrides: {
        dot: {
          width: 10,
          height: 10,
          borderRadius: "50%",
        },
      },
    },
  };
}
