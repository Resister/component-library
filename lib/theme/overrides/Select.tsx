import { InputSelectIcon } from "./CustomIcons";

// ----------------------------------------------------------------------

// todo: add interface
export default function Select(): unknown {
  return {
    MuiSelect: {
      defaultProps: {
        IconComponent: InputSelectIcon,
      },
    },
  };
}
